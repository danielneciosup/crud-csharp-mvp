﻿using System.Collections.Generic;
using CsMantenimientoPersonaUI.TransferObjects;
using BE = CsMantenimientoPersonasBE;

namespace CsMantenimientoPersonaUI.Transformer
{
    class PersonalTransformerToTO : Util.Transformer<BE.PersonalBE, PersonalTO>
    {
        public List<PersonalTO> transformer(List<BE.PersonalBE> lsPersonal)
        {
            List<PersonalTO> lsPersonalTO = new List<PersonalTO>();

            foreach ( BE.PersonalBE personalBE in lsPersonal )
               lsPersonalTO.Add( transformer( personalBE ) );

            return lsPersonalTO;
        }

        public PersonalTO transformer(BE.PersonalBE personal)
        {
            PersonalTO personalTO = null;

            if (personal != null)
            {
                personalTO = new PersonalTO();
                personalTO.Id = personal.Id;
                personalTO.Nombre = personal.Nombre;
                personalTO.Correo = personal.Correo;
            }

            return personalTO;
        }
    }
}
