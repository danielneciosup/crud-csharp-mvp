﻿using System;
using System.Collections.Generic;
using CsMantenimientoPersonaUI.Data.Source.Local;
using CsMantenimientoPersonaUI.TransferObjects;

namespace CsMantenimientoPersonaUI.Data
{
    public class PersonalRepository : Source.PersonalDataSource
    {
        private static PersonalRepository INSTANCE;

        private static PersonalLocalDataSource personalLocalDataSource;

        private PersonalRepository() { }

        public static PersonalRepository GetInstance()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new PersonalRepository();
                personalLocalDataSource = PersonalLocalDataSource.getInstance();
            }

            return INSTANCE;
        }

        public override void DeletePersonal(int personId)
        {
            personalLocalDataSource.DeletePersonal(personId);
        }

        public override void getAllPersonal(OnLoadPersonalesCallback callback)
        {
            personalLocalDataSource.getAllPersonal(new OnLoadPersonalesCallbackImpl(callback));
        }

        public override void getPersonal(OnLoadPersonalCallback callback)
        {
            throw new NotImplementedException();
        }

        public override void SavePersonal(TransferObjects.PersonalTO personal)
        {
            personalLocalDataSource.SavePersonal( personal );
        }

        private class OnLoadPersonalesCallbackImpl : OnLoadPersonalesCallback
        {
            OnLoadPersonalesCallback callback;

            public OnLoadPersonalesCallbackImpl( OnLoadPersonalesCallback callback )
            {
                this.callback = callback;
            }

            public void onDataNotAvailable()
            {
                callback.onDataNotAvailable();
            }

            public void onErrorConnection()
            {
                callback.onErrorConnection();
            }

            public void onLoadPersonal(List<PersonalTO> personales)
            {
                callback.onLoadPersonal(personales);
            }
        }

        private class OnSavePersonalCallbackImpl : OnLoadPersonalCallback
        {
            OnLoadPersonalCallback callback;

            public OnSavePersonalCallbackImpl(OnLoadPersonalCallback callback)
            {
                this.callback = callback;
            }

            public void onDataNotAvailable()
            {
                callback.onDataNotAvailable();
            }

            public void onErrorConnection()
            {
                callback.onErrorConnection();
            }

            public void onLoadPersonal(PersonalTO personal)
            {
                callback.onLoadPersonal(personal);
            }
        }
    }    
}
