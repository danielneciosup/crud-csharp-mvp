﻿using System.Collections.Generic;

namespace CsMantenimientoPersonaUI.Data.Source
{
    public abstract class PersonalDataSource
    {
        public interface OnLoadPersonalCallback
        {
            void onLoadPersonal(TransferObjects.PersonalTO personal);

            void onDataNotAvailable();

            void onErrorConnection();
        }

        public interface OnLoadPersonalesCallback
        {
            void onLoadPersonal(List<TransferObjects.PersonalTO> personales);

            void onDataNotAvailable();

            void onErrorConnection();
        }

        public abstract void getPersonal(OnLoadPersonalCallback callback);

        public abstract void SavePersonal( TransferObjects.PersonalTO personal );

        public abstract void getAllPersonal(OnLoadPersonalesCallback callback);

        public abstract void DeletePersonal(int personId);
    }
}
