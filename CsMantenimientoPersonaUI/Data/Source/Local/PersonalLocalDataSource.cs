﻿using System.Collections.Generic;
using Util;
using DataAccess;
using System.Data;
using NpgsqlTypes;
using CsMantenimientoPersonaUI.TransferObjects;
using System;

namespace CsMantenimientoPersonaUI.Data.Source.Local
{
    public class PersonalLocalDataSource : PersonalDataSource
    {
        public static PersonalLocalDataSource INSTANCE;
        
        private PersonalLocalDataSource(){}

        public static PersonalLocalDataSource getInstance()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new PersonalLocalDataSource();
            }

            return INSTANCE;
        }

        public override void DeletePersonal(int personId)
        {
            object[,] paramSP = new object[,] { { "@id_persona", NpgsqlDbType.Integer, personId } };
            Response response = BaseDao.BaseData("fn_delete_personal", paramSP, AccesoDatos.getConnectionString() );
        }

        public override void getAllPersonal(OnLoadPersonalesCallback callback)
        {
            List<PersonalTO> personales = new List<PersonalTO>();

            object[,] paramSP = new object[,] { { "@idpersonal", NpgsqlDbType.Varchar, "" }, { "@nompersonal", NpgsqlDbType.Varchar, "" }, { "@correopersonal", NpgsqlDbType.Varchar, "" } };
            Response response = BaseDao.BaseData("fn_listarpersonal", paramSP, AccesoDatos.getConnectionString() );

            if (response._Codigo == CodigoError.OK)
            {
                DataTable dt = response._Data;

                if (dt.Rows.Count >= 1)
                {
                    foreach ( DataRow row in dt.Rows )
                    {
                        personales.Add(new PersonalTO { Id = row.Field<int>("Id"), Nombre = row.Field<string>("Nombre"), Correo = row.Field<string>( "Correo" ) } );
                    }
                }

                callback.onLoadPersonal( personales );
            }
        }

        public override void getPersonal(OnLoadPersonalCallback callback)
        {

        }

        public override void SavePersonal(PersonalTO personalTO)
        {
            object[,] paramSP = new object[,] { { "@nom_persona", NpgsqlDbType.Varchar, personalTO.Nombre },
                                                { "@nom_correo", NpgsqlDbType.Varchar, personalTO.Correo  } };

            Response response = BaseDao.BaseData( "fn_guardar_persona", paramSP, AccesoDatos.getConnectionString() );
        }
    }
}
