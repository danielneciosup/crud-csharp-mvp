﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsMantenimientoPersonaUI.TransferObjects
{
    public class PersonalTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }

        public PersonalTO(int Id, string Nombre, string Correo)
        {
            this.Id = Id;
            this.Nombre = Nombre;
            this.Correo = Correo;
        }

        public PersonalTO()
        {
        }
    }
}
