﻿using System.Collections.Generic;
using Util;

namespace CsMantenimientoPersonaUI.Contract
{
    public class PersonalContract
    {
        public interface Presenter : BasePresenter
        {
            void OnCreatePersonal();

            void OnDeletePersonal(int idPersonal);

            void OnLoadPersonal(TransferObjects.PersonalTO personal);

            void OnListPersonal();
        }

        public interface View : BaseView<Presenter>
        {
            void ShowViewCreatePersonal();
            
            void ShowMessageDeleteSuccess();

            void ShowPersonal(TransferObjects.PersonalTO personal);

            void ShowListPersonal(List<TransferObjects.PersonalTO> personales);

            void ShowMessageErrorTransaction();

            void ShowMessageErrorConnection();
        }
    }
}
