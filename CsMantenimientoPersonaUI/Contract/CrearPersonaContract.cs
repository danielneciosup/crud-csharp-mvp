﻿using Util;

namespace CsMantenimientoPersonaUI.Contract
{
    public class CrearPersonaContract
    {
        public interface Presenter : BasePresenter
        {
            void OnSavePerson( TransferObjects.PersonalTO personal );

            void OnIncorrectValues();

            void OnCancel();
        }

        public interface View : BaseView<Presenter>
        {
            void ShowMessageSaveSuccess();

            void ShowMessageErrorService();

            void DisplayCloseWindow();

            void ShowMessageIncorrectValues();
        }
    }
}
