﻿using Util;

namespace CsMantenimientoPersonaUI.Contract
{
    public abstract class EditarPersonaContract
    {
        public interface View : BaseView<Presenter>
        {
            void ShowPersona(TransferObjects.PersonalTO persona);

            void ShowMessageUpdateSuccess();

            void ShowMessageUpdateError();

            void DisplayCloseWindow();            
        }

        public interface Presenter : BasePresenter
        {
            void OnLoadPersona();

            void OnUpdatePersona(TransferObjects.PersonalTO persona);

            void OnCancel();
        }             
    }
}
