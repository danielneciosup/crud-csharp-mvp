﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsMantenimientoPersonaUI.TransferObjects;
using System.Windows.Forms;

namespace CsMantenimientoPersonaUI.Implement.Presenter
{
    public class EditarPersonaPresenter : Contract.EditarPersonaContract.Presenter
    {
        private Contract.EditarPersonaContract.View _view;
        private Data.PersonalRepository _repository;

        public EditarPersonaPresenter( Contract.EditarPersonaContract.View view )
        {
            _view = view;
            _repository = Data.PersonalRepository.GetInstance();
        }

        public void OnLoadPersona()
        {
            throw new NotImplementedException();
        }

        public void OnUpdatePersona(PersonalTO persona)
        {
            throw new NotImplementedException();
        }

        public void OnCancel()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }
    }
}
