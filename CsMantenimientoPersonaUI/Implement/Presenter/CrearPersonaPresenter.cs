﻿using System;
using CsMantenimientoPersonaUI.Data.Source;
using CsMantenimientoPersonaUI.TransferObjects;

namespace CsMantenimientoPersonaUI.Implement.Presenter
{
    public class CrearPersonaPresenter : Contract.CrearPersonaContract.Presenter
    {
        private Contract.CrearPersonaContract.View mView;
        private Data.PersonalRepository mRepository;

        public CrearPersonaPresenter( Contract.CrearPersonaContract.View view )
        {
            mView = view;
            mRepository = Data.PersonalRepository.GetInstance();
        }

        public void OnCancel()
        {
            mView.DisplayCloseWindow();
        }

        public void OnSavePerson(PersonalTO personal)
        {
            if (personal.Nombre.Trim() == "" || personal.Correo.Trim() == "")
                OnIncorrectValues();
            else
            {
                mRepository.SavePersonal(personal);
                mView.ShowMessageSaveSuccess();
                mView.DisplayCloseWindow();
            }
        }

        public void Start()
        {
            
        }

        public void OnIncorrectValues()
        {
            mView.ShowMessageIncorrectValues();
        }
    }
}
