﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsMantenimientoPersonaUI.TransferObjects;
using System.Windows.Forms;

namespace CsMantenimientoPersonaUI.Implement.Presenter
{
    public class PersonalPresenter : Contract.PersonalContract.Presenter
    {
        private Data.PersonalRepository repository;
        private Contract.PersonalContract.View mView;
        private Form mForm;

        public PersonalPresenter(Contract.PersonalContract.View view, Form form)
        {
            this.mView = view;
            this.mForm = form;
            repository = Data.PersonalRepository.GetInstance();
        }

        public void OnCreatePersonal()
        {
            mView.ShowViewCreatePersonal();
        }

        public void OnDeletePersonal(int idPersonal)
        {
            repository.DeletePersonal(idPersonal);
        }

        public void OnListPersonal()
        {
            repository.getAllPersonal(new OnLoadPersonalesCallbackImpl(mView));
        }

        public void OnLoadPersonal(PersonalTO personal)
        {

        }

        public void Start()
        {
            OnListPersonal();
        }

        public void OnWarningDeletePerson()
        {
            
        }

        private class OnLoadPersonalesCallbackImpl : Data.Source.PersonalDataSource.OnLoadPersonalesCallback
        {
            private Contract.PersonalContract.View mView;

            public OnLoadPersonalesCallbackImpl(Contract.PersonalContract.View view)
            {
                mView = view;
            }

            public void onDataNotAvailable()
            {

            }

            public void onErrorConnection()
            {
                mView.ShowMessageErrorConnection();
            }

            public void onLoadPersonal(List<PersonalTO> personales)
            {
                mView.ShowListPersonal(personales);
            }
        }
    }
}
