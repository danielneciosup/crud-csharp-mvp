﻿using System;
using System.Windows.Forms;
using CsMantenimientoPersonaUI.Contract;
using CsMantenimientoPersonaUI.TransferObjects;

namespace CsMantenimientoPersonaUI.Implement.View.PersonalView
{
    public partial class FrmEditPersona : Form, EditarPersonaContract.View
    {
        private EditarPersonaContract.Presenter _presenter;

        public FrmEditPersona()
        {
            InitializeComponent();
            SetPresenter( new Presenter.EditarPersonaPresenter( this ) );
        }

        public void DisplayCloseWindow()
        {
            Dispose();
        }

        public void SetPresenter(EditarPersonaContract.Presenter presenter)
        {
            _presenter = presenter;
        }

        public void ShowMessageUpdateError()
        {
            
        }

        public void ShowMessageUpdateSuccess()
        {
            
        }

        public void ShowPersona(PersonalTO persona)
        {
            
        }
    }
}
