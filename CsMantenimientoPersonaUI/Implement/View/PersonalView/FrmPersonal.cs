﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using CsMantenimientoPersonaUI.Contract;
using CsMantenimientoPersonaUI.TransferObjects;
using System.Drawing;

namespace CsMantenimientoPersonaUI.Implement.View.PersonalView
{
    public partial class frmPersonal : Form, PersonalContract.View
    {
        private PersonalContract.Presenter mPresenter;

        private static readonly string OPTION_EDIT = "Editar";
        private static readonly string OPTION_DELETE = "Eliminar";

        public frmPersonal()
        {
            InitializeComponent();
            dgvPersonal.ReadOnly = true ;
            dgvPersonal.MultiSelect = false;
            dgvPersonal.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        
            SetPresenter( new Presenter.PersonalPresenter( this, this ) );
            mPresenter.Start();
        }

        public void SetPresenter(PersonalContract.Presenter presenter)
        {
            mPresenter = presenter;
        }

        public void ShowListPersonal(List<PersonalTO> personales)
        {
            var lsPersonas = new BindingList<PersonalTO>( personales );
            dgvPersonal.DataSource = lsPersonas;
        }

        public void ShowMessageAlertDelete()
        {

        }

        public void ShowMessageDeleteSuccess()
        {

        }

        public void ShowMessageErrorConnection()
        {

        }

        public void ShowMessageErrorTransaction()
        {

        }

        public void ShowPersonal(PersonalTO personal)
        {
            
        }

        public void ShowViewCreatePersonal()
        {
            frmCrearPersona frmCrearPersona = new frmCrearPersona();
            frmCrearPersona.Show();
        }
        
        private void OnAgregarPersonaClick(object sender, System.EventArgs e)
        {
            mPresenter.OnCreatePersonal();
        }
        
        private void OnRightClick(object sender, MouseEventArgs mEvent)
        {
            if (mEvent.Button == MouseButtons.Right)
            {
                ContextMenuStrip menu = new ContextMenuStrip();
                menu.Items.Add( OPTION_EDIT );
                menu.Items.Add( OPTION_DELETE );
                menu.ItemClicked += OnRightMenuClick;
                
                menu.Show(dgvPersonal, new Point(mEvent.X, mEvent.Y));  
            }
        }

        private void OnRightMenuClick(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            DataGridViewRow row = dgvPersonal.CurrentRow;

            if (item.Text == OPTION_EDIT)
            {

            }
            else if (item.Text == OPTION_DELETE)
            {
                if ( DialogResult.Yes == MessageBox.Show( "¿Desea eliminar el registro?", "Mensaje del sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Warning ) )
                    mPresenter.OnDeletePersonal( int.Parse(row.Cells[0].Value.ToString() ) );
                else
                    Dispose();
            }
        }
    }
}
