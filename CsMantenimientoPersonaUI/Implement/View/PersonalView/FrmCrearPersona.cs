﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CsMantenimientoPersonaUI.Contract;
using CsMantenimientoPersonaUI.TransferObjects;

namespace CsMantenimientoPersonaUI.Implement.View.PersonalView
{
    public partial class frmCrearPersona : Form, CrearPersonaContract.View
    {
        CrearPersonaContract.Presenter mPresenter;

        public frmCrearPersona()
        {
            InitializeComponent();
            SetPresenter( new Presenter.CrearPersonaPresenter( this ) );
        }

        public void DisplayCloseWindow()
        {
            this.Dispose();
        }

        public void SetPresenter(CrearPersonaContract.Presenter presenter)
        {
            mPresenter = presenter;
        }

        public void ShowMessageErrorService()
        {
            MessageBox.Show("Hubo un error. Vuelva a intentarlo más tarde.", "Error");
        }

        public void ShowMessageSaveSuccess()
        {
            MessageBox.Show( "Persona guardada con éxito.", "Éxito" );
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DisplayCloseWindow();
        }

        private void OnAceptarClick(object sender, EventArgs e)
        {
            errFieldEmpty.SetError(txbNombre, null);
            errFieldEmpty.SetError(txbCorreo, null);
            
            mPresenter.OnSavePerson(new PersonalTO { Correo = txbCorreo.Text, Nombre = txbNombre.Text });       
        }

        public void ShowMessageIncorrectValues()
        {            
            if( txbNombre.Text.Trim() == "" )
                errFieldEmpty.SetError( txbNombre, "Nombre no ingresado" );

            if ( txbCorreo.Text.Trim() == "" )
                errFieldEmpty.SetError( txbCorreo, "Correo no ingresado" );
        }
    }
}
