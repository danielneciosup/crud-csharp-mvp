﻿using System;
using System.Configuration;
using System.Windows.Forms;

namespace CsMantenimientoPersonaUI
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            string connectionStringBase = ConfigurationManager.ConnectionStrings["connectionStrings"].ToString();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );

            DataAccess.AccesoDatos.connectionString = connectionStringBase;

            Application.Run( new Implement.View.PersonalView.frmPersonal() );
        }
    }
}