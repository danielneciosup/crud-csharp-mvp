﻿using System;
using System.Data;
using Npgsql;
using Util;
using NpgsqlTypes;

namespace DataAccess
{
    public class BaseDao
    {
        #region Method-Invoke

        //Adexus: Method for getting a data with conection string custom
        public static Response BaseData(string spName, object[,] paramSP, string conexString)
        {
            return Base(spName, paramSP, conexString); ;
        }

        #endregion

        #region BASE

        //Adexus: Method for getting a DataTable object
        private static Response Base(string spName, object[,] paramSP, string conexString)
        {
            using (DataTable DT = new DataTable())
            {
                Response response = null;

                using ( NpgsqlConnection Connection_ = new NpgsqlConnection( conexString ) )
                {
                    using ( NpgsqlCommand SqlCommand_ = new NpgsqlCommand { CommandType = CommandType.StoredProcedure, CommandText = spName, Connection = Connection_ } )
                    {
                        using ( NpgsqlDataAdapter DA = new NpgsqlDataAdapter( SqlCommand_ ) )
                        {
                            try
                            {
                                if (paramSP != null)
                                {
                                    string param;
                                    NpgsqlDbType type;

                                    for ( int i = 0; i < paramSP.GetLength( 0 ); i++ )
                                    {
                                        param = (string) paramSP[ i, 0 ];
                                        type = (NpgsqlDbType) paramSP[i, 1];

                                        if (param != null && param != "")
                                            SqlCommand_.Parameters.Add( param, type ).Value = paramSP[i, 2];                                        
                                    }
                                }

                                DA.Fill(DT);

                                response = new Response { _Codigo = CodigoError.OK, _Mensaje = MensajeError.ERROR_OK, _Data = DT };
                            }
                            catch ( Exception ex )
                            {
                                response = new Response { _Codigo = CodigoError.ERROR, _Mensaje = ex.Message };
                            }
                        }
                    }
                }

                return response;
            }
        }

        #endregion
    }
}
