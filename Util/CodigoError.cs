﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public static class CodigoError
    {
        public const string OK = "200";
        public const string ERROR = "500";
        public const string VALIDACION = "300";
    }
}
