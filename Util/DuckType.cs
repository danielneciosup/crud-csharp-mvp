﻿using Castle.Core.Interceptor;
using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public static class DuckType
    {
        public static T As<T>(object target) where T : class
        {
            return new ProxyGenerator().CreateInterfaceProxyWithoutTarget<T>(new DuckTypingInterceptor(target));
        }

        private class DuckTypingInterceptor : IInterceptor
        {
            private readonly object target;

            public DuckTypingInterceptor(object target)
            {
                this.target = target;
            }

            public void Intercept(IInvocation invocation)
            {
                var methods = target.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public);

                // This should (probably) be cached
                var method = methods.FirstOrDefault(x => IsCompatible(x, invocation.Method));

                if (invocation.GenericArguments != null && invocation.GenericArguments.Length > 0)
                {
                    if (!method.IsGenericMethod)
                        throw MissingMethodException(invocation);

                    method = method.MakeGenericMethod(invocation.GenericArguments);
                }

                if (method == null)
                {
                    var property = TryMatchProperty(invocation);
                    if (property == null)
                        throw MissingMethodException(invocation);

                    var action = (Delegate)property.GetValue(target, null);
                    invocation.ReturnValue = action.DynamicInvoke(invocation.Arguments);
                    return;
                }

                if (method == null)
                    throw MissingMethodException(invocation);

                invocation.ReturnValue = method.Invoke(target, invocation.Arguments);
            }

            private PropertyInfo TryMatchProperty(IInvocation invocation)
            {
                var properties = target.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

                var targetMethod = invocation.GetConcreteMethod();
                var hasReturnValue = targetMethod.ReturnType != typeof(void);

                var actionTypes = new[]
                {
          typeof(Action), typeof (Action<>), typeof (Action<,>), typeof (Action<,,>),
          typeof (Action<,,,>), typeof (Action<,,,,>), typeof (Action<,,,,,>),
        };

                var funcTypes = new[]
                {
          null, typeof (Func<>), typeof (Func<,>), typeof (Func<,,>), typeof (Func<,,,>),
          typeof (Func<,,,,>), typeof (Func<,,,,,>), typeof (Func<,,,,,,>),
        };

                var types = targetMethod.GetParameters().Select(x => x.ParameterType).ToList();

                if (hasReturnValue)
                    types.Add(targetMethod.ReturnType);

                var delegateTypes = hasReturnValue ? funcTypes : actionTypes;
                if (types.Count > delegateTypes.Length)
                    throw new InvalidOperationException("Too many arguments in method. Consider refactor to parameter object");

                var expectedPropertyType = delegateTypes[types.Count].MakeGenericType(types.ToArray());

                return properties.FirstOrDefault(x => x.PropertyType == expectedPropertyType && x.Name == targetMethod.Name);
            }

            private MissingMethodException MissingMethodException(IInvocation invocation)
            {
                return new MissingMethodException(string.Format("Cannot found compatible method {0} on type {1}", invocation.Method.Name, target.GetType().Name));
            }

            private bool IsCompatible(MethodInfo m1, MethodInfo m2)
            {
                // FIXME: handle ref/out paramenters and generic arguments restrictions

                if (m1.Name != m2.Name || m1.ReturnType != m2.ReturnType)
                    return false;

                if (!m1.IsGenericMethod)
                {
                    var parameterTypes1 = m1.GetParameters().Select(p => p.ParameterType);
                    var parameterTypes2 = m2.GetParameters().Select(p => p.ParameterType);

                    return parameterTypes1.SequenceEqual(parameterTypes2);
                }

                return true;
            }
        }
    }
}
