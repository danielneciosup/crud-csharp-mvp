﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public interface Transformer<X, Y>
    {
        Y transformer( X input );

        List<Y> transformer( List<X> inputs );
    }
}
